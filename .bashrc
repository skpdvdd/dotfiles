
# enable vi mode
set -o vi

# command modifications
alias grep='grep --color=auto'
alias more='less'
alias df='df -h'
alias du='du -c -h'
alias mkdir='mkdir -p -v'
alias ls='ls -hF --color=auto --group-directories-first'
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -I'
alias ln='ln -i'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# aliases

alias ..='cd ..'

alias scat='sudo cat'
alias svim='sudo vim'
alias ll='ls -l --group-directories-first'
alias la='ll -A --group-directories-first'

alias sctl='sudo systemctl'
alias susp='sudo systemctl suspend'
alias msleep='xset dpms force off'

alias pac='sudo pacman -S'
alias paci='pacman -Si'
alias pacu='sudo pacman -Syu'
alias pacr='sudo pacman -Rs'
alias pacs='sudo pacman -Ss'

alias en='setxkbmap en_US'
alias de='setxkbmap de'

alias vboxinit='sudo modprobe vboxdrv vboxnetadp vboxnetflt vboxpci'

# nicer ps1
PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;33m\]\$\[\e[m\] \[\e[0m\]'

