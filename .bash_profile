
# envvars
export PAGER=less
export EDITOR=vim
export VISUAL=vim
export BROWSER=links

# colorgcc
export PATH="/usr/lib/colorgcc/bin:$PATH"

# get aliases
source ~/.bashrc

