
Chris' dotfiles
---------------

Dotfiles for archlinux.

Required packages: `i3-wm`, `i3lock`, `dmenu_xft-height`, `feh`, `xss-lock`, `ranger`, `gnome-terminal`, `chromium`, `gvim`, `r`, `gpicview`, `atool`, `highlight`, `evince`, `mediainfo`.
