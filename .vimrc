
" init pathogen ...
execute pathogen#infect()

" ... which will load
" vim-sensible (so assuming that the corresponding settings are already set)
" vim-colors-solarized (solarized colors, only used for gvim) 
" snipmate (quickly insert code snippets via e.g. foo<tab>)
" latex-box (latex plugin (\ll to compile, \le to show errors, \lt for toc)
" ctrlp (file search, <c-p> to open it)

" looks
set background=light
set cursorline

" persistent undo (directory must exist)
set undofile
set undodir=$HOME/.vim/undo
set undolevels=500
set undoreload=5000

" wrap searches
set wrapscan

" show line numbers
set number

" wrap lines, but do not insert newlines
set wrap
set lbr
set textwidth=0 wrapmargin=0

" tab and autoindent = 4 spaces
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" configure incremental search
set hlsearch
set ignorecase
set smartcase

" remap mapleader key                        
let mapleader = ","
let maplocalleader = "\\"

" clear search highlights with leader /
noremap <silent><Leader>/ :nohls<CR>

" move between splits with ctrl + movement keys
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" improve up/down movement on wrapped lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" move code blocks in visual mode via < > keys
vnoremap < <gv
vnoremap > >gv

" make Y behave like other capitals
map Y y$

" type umlauts via ,,{a,o,u,s}
inoremap <Leader><Leader>a ä
inoremap <Leader><Leader>o ö
inoremap <Leader><Leader>u ü
inoremap <Leader><Leader>s ß
inoremap <Leader><Leader>A Ä
inoremap <Leader><Leader>O Ö 
inoremap <Leader><Leader>U Ü 

" use jj as <esc> alternative
inoremap jj <Esc>

" gvim gui config
if has("gui_running")
    :set guioptions-=T
    :set guioptions-=m
    :set guioptions-=r
    colorscheme solarized
else
    :set t_Co=16
endif

" exec ctrlp in mixed mode by default
let g:ctrlp_cmd = 'CtrlPMixed'

