
options(prompt = "R> ")
options(digits = 4)
options(stringsAsFactors = FALSE)
options(show.signif.stars = FALSE)
options("pdfviewer" = "evince")
error = quote(dump.frames("${R_HOME_USER}/errdump", TRUE)

